// Copyright (c) 2020 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"nanomsg.org/go/mangos/v2"
)

func (th *TopicsHandler) consume(ctx mangos.Context, payload []byte) error {
	var consume protos.TopicSubsciptionConsumeRequest
	err := proto.Unmarshal(payload, &consume)
	if err != nil {
		log.Printf("Failed to unmarshall consume request: %v", err)
		return err
	}
	id := consume.GetSubscription()
	subs := subscriptions[id]
	if subs == nil {
		return fmt.Errorf("Subscription not found")
	}

	rec := <-subs.buffer // wait at least one message
	recs := []*protos.Record{&protos.Record{
		Offset:    rec.Offset,
		Partition: rec.Partition,
		Value:     rec.Value,
	}}
	lim := 10 - 1 // TODO: make size configurable
	bufLen := len(subs.buffer)

	if bufLen > 0 {
		for index := 0; index < max(bufLen, lim); index++ {
			// wake me when September ends.. mmmm
			wakeme := make(chan (bool))
			go func() {
				time.Sleep(2 * time.Second)
				wakeme <- true
			}()
			select {
			case rec = <-subs.buffer:
				recs = append(recs, &protos.Record{
					Offset:    rec.Offset,
					Partition: rec.Partition,
					Value:     rec.Value,
				})
			case <-wakeme:
				break
			}
			// rec = <-subs.buffer
		}
	}
	log.Printf("Replying with %d records", len(recs))
	resp := &protos.TopicSubsciptionConsumeResponse{
		Topic:        subs.topic,
		Subscription: subs.id,
		Records:      recs,
	}
	data, err := proto.Marshal(resp)
	if err != nil {
		log.Printf("Failed to marshall consume response: %v", err)
		return err
	}
	err = ctx.Send(data)
	if err != nil {
		log.Printf("Failed to send consume response: %v", err)
		return err
	}
	return nil
}
