// Copyright (c) 2020 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"fmt"
	"strings"
	"time"

	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"nanomsg.org/go/mangos/v2"
)

func (th *TopicsHandler) delete(ctx mangos.Context, payload []byte) error {
	var delete protos.TopicsDeleteRequest
	err := proto.Unmarshal(payload, &delete)
	if err != nil {
		log.Printf("Failed to unmarshall topic names: %v", err)
		return err
	}
	c, err := th.kafka.Controller()
	if err != nil {
		log.Printf("Failed to get controller: %v", err)
		return err
	}
	names := delete.GetNames()
	log.Printf("Deleting %d topics from kafka", len(names))
	res, err := c.DeleteTopics(&sarama.DeleteTopicsRequest{
		Timeout: 5 * time.Second,
		Topics:  names,
	})
	if err != nil {
		return err
	}

	if res.TopicErrorCodes != nil {
		errs := make([]string, 0)
		for name, errc := range res.TopicErrorCodes {
			if errc != sarama.ErrNoError {
				err := fmt.Sprintf("%s: %s", name, errc.Error())
				errs = append(errs, err)
			}
		}
		log.Printf("Received %d errors in response", len(errs))
		if len(errs) > 0 {
			return fmt.Errorf("Failed to delete topics: %s", strings.Join(errs, ", "))
		}
	}

	kafkaTopics, e := th.admin.ListTopics()
	if e != nil {
		log.Printf("Failed to get list topics: %v", e)
		return e
	}

	limit := 20
	for _, name := range names {
		for i := 1; i <= limit; i++ {
			if _, ok := kafkaTopics[name]; !ok {
				break
			}
			log.Infof("Waiting for topic %s to be deleted %d/%d", name, i, limit)
			time.Sleep(1 * time.Second)
			kafkaTopics, e = th.admin.ListTopics()
			if e != nil {
				log.Printf("Failed to get list topics: %v", e)
				return e
			}
		}
	}

	return nil

}
