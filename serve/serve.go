// Copyright (c) 2019 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"encoding/binary"
	"fmt"
	"net/http"
	"os"

	"github.com/Shopify/sarama"
	"github.com/docker/go-connections/tlsconfig"
	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"gopkg.in/yaml.v2"
	"nanomsg.org/go/mangos/v2"
	"nanomsg.org/go/mangos/v2/protocol/rep"
	"nanomsg.org/go/mangos/v2/transport/ws"
)

func die(format string, v ...interface{}) {
	log.Printf(format, v...)
	fmt.Fprintln(os.Stderr, fmt.Sprintf(format, v...))
	os.Exit(1)
}

type SocketHandler interface {
	Handle(sock mangos.Socket) error
}

type TopicsHandler struct {
	kafka sarama.Client
	admin sarama.ClusterAdmin
	rmux  *mux.Router
	port  int
}

func (th *TopicsHandler) Handle(sock mangos.Socket) error {
	ctx, err := sock.OpenContext()
	if err != nil {
		die("Cannot get request: %v", err)
	}
	defer ctx.Close()
	for {
		log.Printf("Receive from socket...")
		req, e := ctx.Recv()

		if e != nil {
			log.Printf("Failed to receive from socket: %v", e)
			ctx.Send([]byte("ERR"))
			continue
		}
		if len(req) < 4 {
			log.Printf("ERROR: Request to /topics have less than 4 bytes")
			ctx.Send([]byte("ERR"))
			//TODO proper error handling
			continue
		}

		header := binary.LittleEndian.Uint32(req[0:4])
		h32 := int32(header)
		methodName := protos.TopicsRequestMethod_name[h32]

		if methodName == protos.TopicsRequestMethod_LIST.String() {
			err := th.list(ctx)
			if err != nil {
				log.Printf("Failed to list topics: %v", err)
				ctx.Send([]byte("ERR"))
				continue
			}
		}

		if methodName == protos.TopicsRequestMethod_CREATE.String() {
			err := th.create(ctx, req[4:])
			if err != nil {
				log.Printf("Failed to create topic: %v", err)
				ctx.Send([]byte("ERR"))
				continue
			}
			e = ctx.Send([]byte("OK"))
			if e != nil {
				log.Printf("Failed to reply: %v", e)
				continue
			}
		}

		if methodName == protos.TopicsRequestMethod_DELETE.String() {
			err := th.delete(ctx, req[4:])
			if err != nil {
				log.Printf("Failed to delete topics: %v", err)
				e = ctx.Send([]byte("ERR"))
				if e != nil {
					log.Printf("Failed to reply: %v", e)
				}
				continue
			}
			e = ctx.Send([]byte("OK"))
			if e != nil {
				log.Printf("Failed to reply: %v", e)
				continue
			}
		}

		if methodName == protos.TopicsRequestMethod_CONSUME.String() {
			err := th.consume(ctx, req[4:])
			if err != nil {
				log.Printf("Failed to consume from subscription: %v", err)
				e = ctx.Send([]byte("ERR"))
				if e != nil {
					log.Printf("Failed to reply: %v", e)
				}
				continue
			}
		}

		if methodName == protos.TopicsRequestMethod_PRODUCE.String() {
			err := th.produce(ctx, req[4:])
			if err != nil {
				log.Printf("Failed to produce to topic: %v", err)
				e = ctx.Send([]byte("ERR"))
				if e != nil {
					log.Printf("Failed to reply: %v", e)
				}
				continue
			}
		}

		if methodName == protos.TopicsRequestMethod_SUBSCRIBE.String() {
			subs, err := th.subscribe(ctx, req[4:])
			if err != nil {
				log.Printf("Failed to subscribe to topic: %v", err)
				e = ctx.Send([]byte("ERR"))
				if e != nil {
					log.Printf("Failed to reply: %v", e)
				}
				continue
			}
			res := &protos.TopicSubscribeResponse{
				Topic:        subs.topic,
				Subscription: subs.id,
			}
			data, e := proto.Marshal(res)
			if e != nil {
				log.Printf("Failed to reply: %v", e)
				continue
			}
			e = ctx.Send(data)
			if e != nil {
				log.Printf("Failed to reply: %v", e)
				continue
			}
		}

	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type Subscription struct {
	consumer sarama.Consumer
	buffer   chan *sarama.ConsumerMessage
	stop     chan bool
	id       string
	topic    string
}

func addHandler(r *mux.Router, port int, path string, handler SocketHandler, par int) {
	log.Printf("adding handler")
	sock, _ := rep.NewSocket()

	url := fmt.Sprintf("ws://localhost:%d/%s", port, path)
	if l, e := sock.NewListener(url, nil); e != nil {
		die("bad listener: %v", e)
	} else if h, e := l.GetOption(ws.OptionWebSocketHandler); e != nil {
		die("bad handler: %v", e)
	} else {
		l.SetOption(ws.OptionWebSocketCheckOrigin, false)
		r.Handle("/"+path, h.(http.Handler))
		l.Listen()
	}
	log.Printf("Run mangos rep")
	for index := 0; index < par; index++ {
		go handler.Handle(sock)
	}
}

func Run(port int) {
	r := mux.NewRouter()
	r.Handle("/", http.RedirectHandler("/public/", http.StatusPermanentRedirect))
	r.Handle("/public", http.RedirectHandler("/public/", http.StatusPermanentRedirect))
	r.PathPrefix("/public/").Handler(http.StripPrefix("/public", (http.FileServer(http.Dir("./public")))))

	conf := sarama.NewConfig()
	tlo := tlsconfig.Options{}

	// TODO : make configurable
	err := yaml.Unmarshal([]byte(`
cafile: /etc/ssl/certs/ca-certificates.crt
`), &tlo)

	// TODO: other defaults to check in order:
	// based on https://www.happyassassin.net/2015/01/12/a-note-about-ssltls-trusted-certificate-stores-and-platforms/
	// /etc/pki/tls/cert.pem
	// /var/lib/ca-certificates/ca-bundle.pem
	// /etc/pki/tls/certs/ca-bundle.crt
	// /etc/pki/tls/certs/ca-bundle.trust.crt

	if err != nil {
		log.WithError(err).Panic("Cannot unmarshall tls config")
	}
	log.Printf("tls options: %v", tlo)
	tlc, err := tlsconfig.Client(tlo)
	if err != nil {
		log.WithError(err).Panic("Cannot configure tls client")
	}
	log.Printf("tls config: %v", tlc)
	conf.Producer.Return.Successes = true
	conf.Net.TLS.Config = tlc
	//TODO make configurable
	conf.Net.TLS.Enable = false

	conf.Version = sarama.V2_0_1_0
	// TODO: should detect version automatically if possible or read from config

	client, err := sarama.NewClient([]string{"kafka:9092"}, conf)
	// TODO add config

	if err != nil {
		log.Fatalf("Failed to connect to kafka, %v", err)
	}
	defer client.Close()
	admin, err := sarama.NewClusterAdminFromClient(client)
	if err != nil {
		log.Fatalf("Failed to connect to kafka, %v", err)
	}
	defer admin.Close()
	th := &TopicsHandler{
		kafka: client,
		admin: admin,
		rmux:  r,
		port:  port,
	}
	addHandler(r, port, "topics", th, 10)
	// addReqHandler(r, port)

	e := http.ListenAndServe(fmt.Sprintf(":%d", port), r)
	die("Http server died: %v", e)
}
