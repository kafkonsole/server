// Copyright (c) 2020 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"nanomsg.org/go/mangos/v2"
)

func (th *TopicsHandler) list(ctx mangos.Context) error {
	// kafkaTopics, e := th.kafka.Topics()
	// if e != nil {
	// 	log.Printf("Failed to request topics from kafka: %v", e)
	// 	return e
	// }
	kafkaTopics, e := th.admin.ListTopics()
	if e != nil {
		log.Printf("Failed to get list topics: %v", e)
		return e
	}
	log.Printf("Received %d topics from kafka, responding to client", len(kafkaTopics))

	topics := &protos.Topics{
		Topics: make([]*protos.Topic, 0),
	}
	for name, _ := range kafkaTopics {
		topics.Topics = append(topics.Topics, &protos.Topic{
			Name: name,
		})
	}
	topicsMarshalled, e := proto.Marshal(topics)

	if e := ctx.Send(topicsMarshalled); e != nil {
		log.Printf("Failed to respond to socket: %v", e)
		return e
	}
	return nil
}
