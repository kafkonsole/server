// Copyright (c) 2020 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"nanomsg.org/go/mangos/v2"
)

func (th *TopicsHandler) produce(ctx mangos.Context, payload []byte) error {
	var produce protos.TopicProduceRequest
	err := proto.Unmarshal(payload, &produce)
	if err != nil {
		log.WithError(err).Errorf("Failed to unmarshall produce request")
		return err
	}
	topic := produce.GetTopic()
	value := produce.GetValue()
	producer, err := sarama.NewSyncProducerFromClient(th.kafka)
	if err != nil {
		log.WithError(err).Errorf("Failed to create producer")
		return err
	}
	defer producer.Close()
	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.ByteEncoder(value),
	}
	log.Infof("Producing message of %d bytes to %s", len(value), topic)
	partition, offset, err := producer.SendMessage(msg)

	res := &protos.TopicProduceResponse{
		Topic:     topic,
		Partition: partition,
		Offset:    offset,
	}
	data, err := proto.Marshal(res)
	if err != nil {
		log.WithError(err).Errorf("Failed to marshall produce response")
		return err
	}
	err = ctx.Send(data)
	if err != nil {
		log.WithError(err).Errorf("Failed to send produce response")
		return err
	}
	return nil
}
