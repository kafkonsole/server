// Copyright (c) 2020 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"fmt"
	"time"

	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"nanomsg.org/go/mangos/v2"
)

func (th *TopicsHandler) create(ctx mangos.Context, payload []byte) error {
	var create protos.TopicCreateRequest
	err := proto.Unmarshal(payload, &create)
	if err != nil {
		log.Printf("Failed to unmarshall topic name: %v", err)
		return err
	}
	name := create.GetName()
	log.Infof("Creating topic %s", name)
	controller, err := th.kafka.Controller()
	if err != nil {
		log.Printf("Failed to create topic %s: %v", name, err)
		return err
	}
	res, err := controller.CreateTopics(&sarama.CreateTopicsRequest{
		Timeout: 5 * time.Second,
		TopicDetails: map[string]*sarama.TopicDetail{
			name: &sarama.TopicDetail{
				ReplicationFactor: 1,
				NumPartitions:     1,
			}, //TODO add parameters
		},
	})
	if err != nil {
		return err
	}
	if res.TopicErrors != nil {
		resForTopic := res.TopicErrors[name]
		if resForTopic.Err != sarama.ErrNoError {
			return fmt.Errorf("Failed to create topic %s: %s", name, resForTopic.Error())
		}
	}

	kafkaTopics, e := th.admin.ListTopics()
	if e != nil {
		log.Printf("Failed to get list topics: %v", e)
		return e
	}

	limit := 20
	for i := 1; i <= limit; i++ {
		if _, ok := kafkaTopics[name]; ok {
			break
		}
		log.Infof("Waiting for topic %s to be created %d/%d", name, i, limit)
		time.Sleep(1 * time.Second)
		kafkaTopics, e = th.admin.ListTopics()
		if e != nil {
			log.Printf("Failed to get list topics: %v", e)
			return e
		}
	}

	return nil
}
