// Copyright (c) 2020 Timur Sultanaev
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package serve

import (
	"reflect"

	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kafkonsole/server/protos"
	"nanomsg.org/go/mangos/v2"
)

var (
	//TODO: think how to do scalable
	subscriptions = make(map[string]*Subscription)
)

func (th *TopicsHandler) subscribe(ctx mangos.Context, payload []byte) (*Subscription, error) {
	var subscribe protos.TopicSubscribeRequest
	err := proto.Unmarshal(payload, &subscribe)
	if err != nil {
		log.Printf("Failed to unmarshall topic subscription: %v", err)
		return nil, err
	}
	topic := subscribe.GetTopic()
	consumer, err := sarama.NewConsumerFromClient(th.kafka)
	if err != nil {
		return nil, err
	}
	closeConsumer := true
	defer func() {
		if closeConsumer {
			consumer.Close()
		}
	}()
	id := uuid.New().String()

	subscription := &Subscription{
		consumer: consumer,
		buffer:   make(chan *sarama.ConsumerMessage, 10),
		stop:     make(chan bool),
		id:       id,
		topic:    topic,
	}
	partitions, err := th.kafka.Partitions(topic)
	if err != nil {
		return nil, err
	}

	cases := make([]reflect.SelectCase, len(partitions))
	for i, partition := range partitions {
		pc, err := consumer.ConsumePartition(topic, partition, 0)
		if err != nil {
			return nil, err
		}
		channel := pc.Messages()
		cases[i] = reflect.SelectCase{
			Dir:  reflect.SelectRecv,
			Chan: reflect.ValueOf(channel),
		}
	}

	go func() {
		for {
			_, value, _ := reflect.Select(cases)
			// ch := chans[chosen]
			msg := value.Interface().(*sarama.ConsumerMessage)
			select {
			case subscription.buffer <- msg:
				log.Printf("Subscription %s buffers message", id) //TODO leveled log
			case <-subscription.stop:
				log.Printf("Subscription %s stopped", id)
				break
			}
		}
	}()

	subscriptions[id] = subscription
	closeConsumer = false
	return subscription, nil
	//TODO: figure out how to read from buffer (pub/sub?)

}
