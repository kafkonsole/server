# Kafkonsole

## What it is 

Kafkonsole is a project designed to provide developers a deeper
inside into Kafka topics and consumer groups and give ability 
to do common operations (f.e. publish test messages, move offsets, etc). 

## What it is not

There is no goal to support any possible interaction with Kafka, 
such as streams or connectors. 

Tool is designed to be used in a dev environment, hence no 
additional security is provided. 

Tool is not designed to help administrators of Kafka. It could be 
used this way, but development will never specifically target 
administration stories.

## Status

IN (more or less) ACTIVE DEVELOPMENT

Features:
 - [+] list topics
 - [+] publish text records
 - [+] see recors via infinite consumer
 - [+] delete topics (bulk)
 - [ ] configure (multiple) clusters
 - [ ] list groups
 - [ ] list group (active) consumers
 - [ ] show group offsets
 - [ ] template based publishing (text templates, xsd, and binary: f.e. protobuf)
 - [ ] other consuming strategies (from specific offset, from date, paging, etc)
 - [ ] advanced records view (few topics at the same screen)
 - [ ] status/stats?

There's a few known issues:
 - some features have not critical bugs
 - leaks are possible (sometimes requre backed restart)
 - golang in webassembly periodically crashes (requires frontend restart)
 - mobile should mostly be supported thanks to Material UI, but it's not tested very much

## Architecture

Kafkonsole consists of three parts:
 - kafkonsole-ui
 - kafkonsole-client
 - kafkonsole-server

### UI

UI is single page progressive web application (React+Material UI), written 
in Typescript, uses kafkonsole-client to communicate with a backend. 
UI uses MobX to keep frontend state.

### Client

Client is written on Golang, compiled to Web Assembly and run in UI.
Communication with backend is done via nanomsg (mangos) via Websocket.
It's planned also to provide cli client, because usual CLI's 
for kafka are extremly uncomfortable. 

### Server

Server is written on Golang, built in docker, could be deployed to kubernetes
via helm. Server connects to Kafka directly (no REST or other proxies) and 
supports TLS (standart truststore, should be compatible with f.e. AWS MSK).
Kafconsole server serves files with pre-built UI and Websocket/nng connection
for client to connect to.

### Devops

# Roadmap

Support all planned features( see [in status](#Status) )

Stabilize:
 - [ ] cover all features in e2e tests
 - [ ] cover backend with units to 70 percent
 - [ ] improve pipeline stability (fix server when client/ui artifacts are not available)
 - [ ] make e2e test faster

Other:
 - [ ] deploy demo (backend - GCP? or stub the server?)
 - [ ] publish site+docs (Gitlab Pages)
