module gitlab.com/kafkonsole/server

go 1.13

require (
	github.com/Shopify/sarama v1.24.1
	github.com/docker/go-connections v0.4.0
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v2 v2.2.7
	nanomsg.org/go/mangos/v2 v2.0.6
)
