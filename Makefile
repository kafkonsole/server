all: golang docker

golang:
	CGO_ENABLED=0 GOOS=linux go build -o kafkonsole 

docker:
	docker build -t registry.gitlab.com/kafkonsole/server .

cyop:
	( cd tests ; npx cypress open )

cyrun:
	( cd tests ; npx cypress run )
