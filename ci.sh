#!/bin/bash
# Copyright (c) 2019 Timur Sultanaev
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


cd /chart
helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
helm install --wait kafka incubator/kafka --set replicas=1 --set zookeeper.replicaCount=1
kubectl get pods
helm install --wait testrelease .
kubectl get pods

cd /tests
# TODO: add npm to the node image
# apt-get update
# apt-get install -y npm

# TODO: see if helm test would be possible to use 
# , maybe with https://github.com/kubernetes/kubectl/issues/454
# , so far dead can't talk

# npm install

svcIP="$(kubectl get svc testrelease-kafkonsole -o go-template='{{.spec.clusterIP}}')"

COMMIT_INFO_BRANCH=${COMMIT_INFO_BRANCH} \
CYPRESS_BASE_URL="http://${svcIP}:8080" \
CYPRESS_DASHBOARD_KEY=${CYPRESS_DASHBOARD_KEY} \
npm run test:ci

echo "test finished, test report:"
report="mochawesome-report/mochawesome.json"
if [ -f "${report}" ] ; then 
    cat "${report}"
else 
    echo "report not found!"
fi

kubectl describe pod
POD_NAME=$(kubectl get pod -l 'app.kubernetes.io/name=kafkonsole' -o go-template='{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo podname:"${POD_NAME}"
kubectl logs "${POD_NAME}"